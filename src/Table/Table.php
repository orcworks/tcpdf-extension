<?php

namespace ORCWorks\Tcpdf\Table;

use Illuminate\Support\Facades\Log;

class Table
{
    protected array $rows = [];
    protected int $borderWidth = 0;
    protected string $borderColor = '#000000';
    protected int $width;
    protected int $fontSize = 16;
    protected string $fontFamily;
    /**
     * @param bool $isHeader
     * @return Row
     */
    public function newRow(bool $isHeader = false) {
        $row = new Row($this, $isHeader);
        return $this->rows[] = $row;
    }

    public function setBorder(int $width, $color = '#000000') {
        $this->borderWidth = $width;
        $this->borderColor = $color;
    }

    /**
     * 按像素宽度，必须输入
     * @param int $width
     */
    public function setWidth(int $width) {
        $this->width = $width;
    }

    public function setFontSize(int $fontSize) {
        $this->fontSize = $fontSize;
        return $this;
    }

    public function getFontSize() {
        return $this->fontSize;
    }

    /**
     * @return string
     */
    public function getFontFamily(): ?string
    {
        return $this->fontFamily ?? null;
    }

    /**
     * @param string $fontFamily
     * @return Table
     */
    public function setFontFamily(string $fontFamily): Table
    {
        $this->fontFamily = $fontFamily;
        return $this;
    }

    public function toHtml() {
        $html = '<table';
        if ($this->borderWidth) {
            $html .= ' border="' . $this->borderWidth . '"';
        }
        $styles = [];
        if ($this->fontSize) {
            $styles[] = 'font-size: ' . $this->fontSize . 'px';
        }
        if ($this->getFontFamily()) {
            $styles[] = 'font-family: ' . $this->getFontFamily();
        }
        if (count($styles)) {
            $html .= ' style="' . implode('; ', $styles) . '"';
        }
        $html .= ">\n";

        //首先梳理一下
        $rawCells = [];
        $rowIndex = 0;
        foreach ($this->rows as $row) {
            $cells = $row->getCells();
            if (!isset($rawCells[$rowIndex])) {
                $rawCells[$rowIndex] = [];
            }
            $colIndex = 0;
            if (isset($rawCells[$rowIndex])) {
                //说明已经有rowspan。如果rowspan的格子在前面，则需要把colIndex往后延
                //直到找到第一个没有设置过的
                while(isset($rawCells[$rowIndex][$colIndex])) {
                    $colIndex ++;
                }
            }
            /**
             * @var Cell $cell
             */
            foreach ($cells as $cell) {
                for ($i = 0; $i < $cell->getRowSpan(); $i ++) {
                    if (!isset($rawCells[$rowIndex + $i])) {
                        $rawCells[$rowIndex + $i] = [];
                    }
                    if (!isset($rawCells[$rowIndex + $i][$colIndex])) {
                        $rawCells[$rowIndex + $i][$colIndex] = $ca = new CellAttributes($cell);
                    } else {
                        $ca = $rawCells[$rowIndex + $i][$colIndex];
                    }
                    $ca->setRealX($colIndex);
                    $ca->setRealY($rowIndex);
                    $ca->setX($colIndex);
                    $ca->setY($rowIndex + $i);
                }
                for ($i = 0; $i < $cell->getColspan(); $i ++) {
                    if (!isset($rawCells[$rowIndex][$colIndex + $i])) {
                        $rawCells[$rowIndex][$colIndex + $i] = $ca = new CellAttributes($cell);
                    } else {
                        $ca = $rawCells[$rowIndex][$colIndex + $i];
                    }
                    $ca->setRealX($colIndex);
                    $ca->setRealY($rowIndex);
                    $ca->setX($colIndex + $i);
                    $ca->setY($rowIndex);
                }
                $colIndex += $cell->getColspan();
            }
            $rowIndex ++;
        }

        /**
         * @var Row $row
         */
        $rowIndex = 0;
        foreach ($this->rows as $row) {
            $cells = $row->getCells();
            $html .= "\t<tr";
            //TODO row attribute
            $html .= '>';
            /**
             * @var Cell $cell
             */
            foreach ($cells as $cell) {
                $prefixText = '';
                $cellTag = 'td';
                if ($row->isHeader()) {
                    $cellTag = 'th';
                }
                $html .= "\t\t<" . $cellTag;
                if ($cell->getRowSpan() > 1) {
                    $html .= ' rowspan="' . $cell->getRowSpan() . '"';
                }
                if ($cell->getColspan() > 1) {
                    $html .= ' colspan="' . $cell->getColspan() . '"';
                }
                $styles = ['width: ' . $cell->getWidth() . 'px'];
                if ($cell->getAlign() != 'left') {
                    $styles[] = 'text-align: ' . $cell->getAlign();
                }
                $html .= ' style="' . implode(';', $styles) . '"';
                //如果有VAlign
                if ($cell->getVAlign() != 'top') {
                    $totalHeight = 0;
                    //需要计算最大高度
                    for($i = 0; $i < $cell->getRowSpan(); $i ++) {
                        $rowHeight = 0;
                        /**
                         * @var CellAttributes $ca
                         */
                        foreach ($rawCells[$rowIndex + $i] as $ca) {
                            if (!$ca->isRealCell()) {
                                //说明不是存在的cell，要考虑两种情况
                                //如果这个cell没有跨行，则不管
                                continue;
                                //如果cell跨行，TODO
                            }
                            if ($ca->getCell()->getRowSpan() > 1) {
                                //TODO, 当前格子大于一行，默认认为地方够大，实际上要计算rowspan对应的所有格子的高度再累加，可能会造成死循环
                                continue;
                            }
                            $height = $ca->getCellHeight();
                            if ($rowHeight < $height) {
                                $rowHeight = $height;
                            }
//                            if ($rowIndex == 21) {
//                                Log::debug($cell->getText() . ' ' . $height . ' ' . $i . ' ' . $ca->getCell()->getText());
//                            }
                        }
                        $totalHeight += $rowHeight;
                    }
                    //计算本身所占的高度
                    $cellMargins = $cell->getMargin();
                    $textHeight = ($cell->getFontSize() * 1.25) * ceil(mb_strwidth($cell->getText()) * $cell->getFontSize() / (2 * ($cell->getWidth() - $cellMargins['left'] - $cellMargins['right'])));
                    $topHeight = 0;
                    switch ($cell->getVAlign()) {
                        case 'middle':
                            //需要计算多少个br
                            $topHeight = ($totalHeight - $textHeight) / 2;
                            break;
                        case 'bottom':
                            $topHeight = $totalHeight - $textHeight;
                            break;
                    }
                    if ($topHeight > 0) {
                        //以1/4个fontSize为单位
                        $miniFontSize = floor($cell->getFontSize() / 4);
                        $brNums = intval(floor($topHeight / ($miniFontSize * 1.25))) - 1;
//                                dd($topHeight, $totalHeight, $textHeight, $brNums);
                        if ($brNums > 0) {
                            $prefixText = '<span style="width:100%;font-size: ' . $miniFontSize . ';margin:0px;padding:0px;border:1px;display:block;height:' . $topHeight . 'px">' . str_repeat('<br />', $brNums) . '</span>';
                        }
                    }
                }
                if ($cell->getExtraAttributes()) {
                    $html .= ' ' . $cell->getExtraAttributes();
                }
                $html .= '>' . $prefixText . $cell->getText() . '</' . $cellTag . ">\n";
            }
            $html .= '</tr>';
            $rowIndex ++;
        }

        $html .= '</table>';
        return $html;
    }
}