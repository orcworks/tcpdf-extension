<?php

namespace ORCWorks\Tcpdf\Table;

class Cell
{
    protected int $rowspan = 1;
    protected int $colspan = 1;
    protected string $text = '';
    protected string $align = 'left';
    protected string $valign = 'top';
    protected int $fontSize;
    protected int $width;
    protected array $margin = ['top' => 0, 'bottom' => 0, 'left' => 4, 'right' => 4];//默认的margin
    protected string $extraAttributes = '';
    protected Row $row;

    public function __construct(Row $row, $text = null) {
        $this->row = $row;
        if ($text !== null) {
            $this->setText($text);
        }
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Cell
     */
    public function setWidth(int $width)
    {
        $this->width = $width;
        return $this;
    }

    public function setRowspan(int $rowspan) {
        $this->rowspan = $rowspan;
        return $this;
    }

    public function getRowSpan() {
        return $this->rowspan;
    }

    public function setColspan(int $colspan) {
        $this->colspan = $colspan;
        return $this;
    }

    public function getColspan() {
        return $this->colspan;
    }

    public function setText(string $text) {
        $this->text = $text;
        return $this;
    }

    public function getText() {
        return $this->text;
    }

    public function setAlign($align) {
        if (!in_array($align, ['left', 'center', 'right'])) {
            throw new \Exception('align值不正确');
        }
        $this->align = $align;
        return $this;
    }

    public function getAlign() {
        return $this->align;
    }

    public function setVAlign($valign) {
        if (!in_array($valign, ['top', 'middle', 'bottom'])) {
            throw new \Exception('valign值不正确');
        }
        $this->valign = $valign;
        return $this;
    }

    public function getVAlign() {
        return $this->valign;
    }

    /**
     * @return array|int[]
     */
    public function getMargin(): array
    {
        return $this->margin;
    }

    /**
     * @param array|int[] $margin
     * @return Cell
     */
    public function setMargin(array $margin): Cell
    {
        $this->margin = $margin;
        return $this;
    }

    /**
     * @return int
     */
    public function getFontSize(): int
    {
        if (isset($this->fontSize)) {
            return $this->fontSize;
        }
        return $this->row->getTable()->getFontSize();
    }

    /**
     * @param int $fontSize
     * @return Cell
     */
    public function setFontSize(int $fontSize): Cell
    {
        $this->fontSize = $fontSize;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtraAttributes(): string
    {
        return $this->extraAttributes;
    }

    /**
     * 设置额外属性
     * @param string $extraAttributes
     * @return Cell
     */
    public function setExtraAttributes(string $extraAttributes): Cell
    {
        $this->extraAttributes = $extraAttributes;
        return $this;
    }


}