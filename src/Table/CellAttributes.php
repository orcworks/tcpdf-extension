<?php

namespace ORCWorks\Tcpdf\Table;

class CellAttributes
{
    protected Cell $cell;
    protected int $x;
    protected int $y;
    protected int $realX;
    protected int $realY;

    public function __construct(Cell $cell) {
        $this->cell = $cell;
    }

    /**
     * 是不是就是本格还是rowspan等造成的占位格
     * @return bool
     */
    public function isRealCell() {
        return ($this->x == $this->realX && $this->y = $this->realY);
    }

    public function getCell() {
        return $this->cell;
    }
    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     * @return CellAttributes
     */
    public function setX(int $x): CellAttributes
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return CellAttributes
     */
    public function setY(int $y): CellAttributes
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return int
     */
    public function getRealX(): int
    {
        return $this->realX;
    }

    /**
     * @param int $realX
     * @return CellAttributes
     */
    public function setRealX(int $realX): CellAttributes
    {
        $this->realX = $realX;
        return $this;
    }

    /**
     * @return int
     */
    public function getRealY(): int
    {
        return $this->realY;
    }

    /**
     * @param int $realY
     * @return CellAttributes
     */
    public function setRealY(int $realY): CellAttributes
    {
        $this->realY = $realY;
        return $this;
    }

    /**
     * 检测当前格子内容的行数
     * @return int|void
     */
    public function getCellLines() {
        if (!$this->isRealCell()) {
            return 0;
        }
        $cellMargins = $this->getCell()->getMargin();
        $lines = ceil(mb_strwidth($this->getCell()->getText()) * $this->getCell()->getFontSize() / (2 * ($this->getCell()->getWidth() - $cellMargins['left'] - $cellMargins['right'])));
        return (int) $lines;
    }

    public function getCellHeight() {
        return ($this->getCell()->getFontSize() * 1.25) * $this->getCellLines();
    }
}