<?php

namespace ORCWorks\Tcpdf\Table;

class Row
{
    protected Table $table;
    protected bool $isHeader;
    protected array $cells = [];


    public function __construct(Table $table, bool $isHeader = false) {
        $this->table = $table;
        $this->isHeader = $isHeader;
    }

    public function newCell($text = null) {
        $cell = new Cell($this, $text);
        return $this->cells[] = $cell;
    }

    public function getCells() {
        return $this->cells;
    }

    public function isHeader() {
        return $this->isHeader;
    }

    /**
     * @return Table
     */
    public function getTable(): Table
    {
        return $this->table;
    }


}