$table = new Table();
$table->setBorder(1);

$row = $table->newRow();
$cell = $row->newCell();
$cell->setRowspan(2);
...
...
$html = $table->toHtml();

然后就可以把$html写进pdf了。